### What is this repository for? ###

* An example that shows how to create more than multiple VMs adding them to an existing resource group, VNet, and Subnet.
* Does not create a new VNet, Subnet, Public IP, and NSG group. Therefore should work for users without Network Contributor permissions.
* Assumes user has VM Contributor permission.

### Can I run this from the Azure CLI? ###

* az login
* az group deployment create \
    --name CentOSVMDeployment \
    --resource-group centosvmss \
    --template-uri "https://bitbucket.org/jkarasha/arm-template-centos-vmms/201-vm-different-rg-vnet/azuredeploy.json""


### Can I run this using PowerShell? ###

* Coming soon..

### Can I run this from Azure Console? ###

* Coming soon..

### Who do I talk to? ###

* jkarasha@gmail.com
